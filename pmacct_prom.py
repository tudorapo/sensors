#!/usr/bin/env python3
"""Serving pmacct to prometheus"""

from subprocess import Popen, PIPE, DEVNULL
from threading import Thread
from http.server import BaseHTTPRequestHandler, HTTPServer
from shlex import split
import logging
import sys
import argparse
import socket
import json

logging.basicConfig(level=logging.INFO)
parser = argparse.ArgumentParser(description='Turbostat for prometheus.')
parser.add_argument('-a', '--address', default='0.0.0.0',
                    help='ip address where to serve')
parser.add_argument('-p', '--port', default='9104',
                    help='Port where to serve')
parser.add_argument('-i', '--interface', default='anyad0',
                    help='Interface to sniff')
parser.add_argument('-n', '--network', default='192.168.0.0/16',
                    help='Network filter, ip4, format is 192.168.0.0/16')
args = parser.parse_args()

TITLE = 'pmacct'

DATA = {}

class Metrics(BaseHTTPRequestHandler):
    """Simple webserver."""
    def hwrite(self, msg):
        """for convenience."""
        self.wfile.write(bytes(msg, 'utf-8'))
    def log_request(self, code='-', size='-'):
        if code != 200:
            self.log_message('jaj! "%s" %s %s',self.requestline, str(code), str(size))
    def do_GET(self):
        """http reply."""
        global DATA
        if DATA:
            self.send_response(200)
            self.send_header('Content-type', 'text/plain; version=0.0.4; charset=utf-8')
            self.end_headers()
            self.hwrite(f'# HELP { TITLE }_src_bytes sent bytes\n')
            self.hwrite(f'# TYPE { TITLE }_src_bytes counter\n')
            self.hwrite(f'# HELP { TITLE }_dst_bytes received bytes\n')
            self.hwrite(f'# TYPE { TITLE }_dst_bytes counter\n')
            self.hwrite(f'# HELP { TITLE }_src_packets sent packages\n')
            self.hwrite(f'# TYPE { TITLE }_src_packets counter\n')
            self.hwrite(f'# HELP { TITLE }_dst_packets received packages\n')
            self.hwrite(f'# TYPE { TITLE }_dst_packets counter\n')
            for i, direction in enumerate(DATA):
                for n, name in enumerate(DATA[direction]):
                    self.hwrite(f'{ TITLE }_{direction}_bytes{{name="{name}"}} {DATA[direction][name]["byte"]} \n')
                    self.hwrite(f'{ TITLE }_{direction}_packets{{name="{name}"}} {DATA[direction][name]["packet"]} \n')
        else:
            self.send_response(503)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.hwrite('Wait a bit \n')

def web():
    """Simple webserver thread."""
    server = HTTPServer((args.address, int(args.port)), Metrics)
    logging.info('webserver starts at %s/%s', args.address, args.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()


def store(direction, name, packet, byte):
    """Storing one measure from the pmacct threads"""
    global DATA
    logging.debug(f'store {direction} {name} {packet} {byte}')
    if direction not in DATA:
        DATA[direction] = {}
    if name not in DATA[direction]:
        DATA[direction][name] = {'packet': 0, 'byte': 0}
    DATA[direction][name]['packet'] += packet
    DATA[direction][name]['byte'] += byte

def pmacct(direction):
    """Running and receiving pmacct"""
    cmd = f'pmacctd -d -P print -O json -r 5 -i {args.interface} -c {direction}_host {direction} net {args.network}'
    logging.info(f'pmacct for {direction} starts')
    logging.debug(cmd)
    with Popen(split(cmd), stdout=PIPE, stderr=DEVNULL, universal_newlines=True) as proc:
        for line in iter(proc.stdout.readline, ""):
            js = json.loads(line)
            ip = js[f'ip_{direction}']
            try:
                name = socket.gethostbyaddr(ip)[0].split('.')[0]
            except BaseException as e:
                name = ip
                logging.warning(f'nafene {ip}')
                print(e)
            try:
                store(direction, name, js['packets'], js['bytes'])
            except BaseException as e:
                logging.error('mifene')
                print(e)
        proc.kill()
    logging.info(f'pmacct for {direction} ends')

if __name__ == "__main__":
    try:
        pm_src = Thread(target=pmacct, args=['src'])
        pm_src.daemon = True
        pm_src.start()
        pm_dst = Thread(target=pmacct, args=['dst'])
        pm_dst.daemon = True
        pm_dst.start()
        web = Thread(target=web)
        web.daemon = True
        web.start()
        while pm_src.is_alive() and pm_dst.is_alive() and web.is_alive():
            pm_src.join(1)
    except KeyboardInterrupt:
        logging.info('pmacct ends')
        sys.exit(1)
