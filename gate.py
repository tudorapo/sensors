from time import sleep
from gpiozero import LED, Button

led = LED(27)
button = Button(18)

led.off()

def on():
    led.off()
    print "on"

def off():
    led.on()
    print "off"

button.when_pressed = on
button.when_released = off
