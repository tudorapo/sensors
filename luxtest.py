#!/usr/bin/python2

import smbus
import time
import math

bus = smbus.SMBus(1)
#this is the poweron, do not touch it.

bus.write_byte_data(0x39, 0x00 | 0x80, 0x03)

#stufff copied from https://github.com/adafruit/Adafruit_CircuitPython_TSL2561/blob/master/adafruit_tsl2561.py
_GAIN_SCALE = (16, 1)
_TIME_SCALE = (1 / 0.034, 1 / 0.252, 1)
_CLIP_THRESHOLD = (4900, 37000, 65000)

def compute_lux(ch0, ch1, ctime, gain):
        """Based on datasheet for FN package."""
        if ch0 == 0:
            return None
        if ch0 > _CLIP_THRESHOLD[ctime]:
            return None
        if ch1 > _CLIP_THRESHOLD[ctime]:
            return None
        ratio = ch1 / ch0
        if ratio >= 0 and ratio <= 0.50:
            lux = 0.0304 * ch0 - 0.062 * ch0 * ratio**1.4
        elif ratio <= 0.61:
            lux = 0.0224 * ch0 - 0.031 * ch1
        elif ratio <= 0.80:
            lux = 0.0128 * ch0 - 0.0153 * ch1
        elif ratio <= 1.30:
            lux = 0.00146 * ch0 - 0.00112 * ch1
        else:
            lux = 0.
        # Pretty sure the floating point math formula on pg. 23 of datasheet
        # is based on 16x gain and 402ms integration time. Need to scale
        # result for other settings.
        # Scale for gain.
        lux *= _GAIN_SCALE[gain]
        # Scale for integration time.
        lux *= _TIME_SCALE[ctime]
        return lux

#this is the sensitivity:
# first two bits are the time used to collect the light,
# third bit is the sensitivity
# 000 =  13.7 ms, 1x
# 001 = 101   ms, 1x
# 010 = 402   ms, 1x
# 011 = manual, 1x
# 100 =  13.7 ms, 16x
# 101 = 101 ms, 16x
# 110 = 402 ms, 16x
# 111 = manual, 16x

luxus = []

for gain in range(2):
	for ctime in range(3):
		code = ctime + (gain * 16)
		bus.write_byte_data(0x39, 0x01 | 0x80, code)
		time.sleep(0.5)
		bband = bus.read_i2c_block_data(0x39, 0x0C | 0x80, 2)
		infra = bus.read_i2c_block_data(0x39, 0x0E | 0x80, 2)
		ch0 = bband[1] * 256 + bband[0]
		ch1 = infra[1] * 256 + infra[0]
		lux = compute_lux(ch0, ch1, ctime, gain)
		if lux:
			luxus.append(lux)
print reduce(lambda x, y: x + y, luxus) / float(len(luxus))

		
