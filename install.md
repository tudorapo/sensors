# How to install this thing?
## Directores

It needs two tmpfs mounts, both owned by pi:

   - `/still` - this will be the temporary directory where pictures to be uploaded will be stored. Written often, use tmpfs. It's size limits the amount of pictures to be taken during one event, as the copy is slower than taking new pictures. It's configurable in motion.cfg as spool dir.
   - `/var/www` - this is where raspistill puts the pictures it takes, this is where it's copied away by motion. This should be the webroot.
   - `/home/pi/sensors` - it is supposed that the project git@gitlab.com:tudorapo/sensors.git is downloaded there.

Relevant `/etc/fstab` sniffet:

```
tmpfs /var/www tmpfs rw,nodev,nosuid,size=2M,uid=pi  0  0
tmpfs /still   tmpfs rw,nodev,nosuid,size=50M,uid=pi 0  0
```

## Packages
   - nginx (or any webserver)
   - runit 
   - munin-node
   - python-paramiko
   - python-inotifyx
   - python-configargparse
   - python-gpiozero
   - python-pip
   - mem_top (via pip)

## Assorted other stuff

   - `ssh-keygen -t rsa -f .ssh/sftp_key` - this key is needed for copying the images. Copy the pu to the target host. Put the path to the config file.
   - [99-com.rules](https://github.com/RPi-Distro/raspberrypi-sys-mods/blob/master/etc.armhf/udev/rules.d/99-com.rules) - necessary for the permissions to be set for gpio. Put it under `/etc/udev/rules.d/`
   - `ln -s /home/pi/sensors/muninplugin.sh exttemp` - thus we can have continuity in temperature diagrams.
   - openvpn, don't forget to add the tomka.hu ip to `munin-node.conf`!
   - currently the logs are set up to go to the gateway, open port 514.

