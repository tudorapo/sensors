#!/bin/sh
#%# family=auto
#%# capabilities=autoconf



if [ "$1" = "autoconf" ]; then
        echo yes 
        exit 0
fi

if [ "$1" = "config" ]; then

        echo 'graph_title Ambient temperature'
        echo 'graph_vlabel C' 
        echo 'graph_category sensors'
        echo 'graph_info Temperature outside.'
        echo 'temp.label temp'
        echo 'temp.draw LINE2'
        echo 'temp.info Temperature in celsius.'
        exit 0
fi

echo -n "temp.value "
/home/pi/sensors/gettemp.py
