#!/usr/bin/env python3

from PIL import ImageFont
import inkyphat
import psycopg2
import argparse

font_file = '/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf'
font_size = 8
border_bottom = 2

parser = argparse.ArgumentParser()
parser.add_argument('typ')
parser.add_argument('sen')
args = parser.parse_args()

#Collecting data
conn = psycopg2.connect("dbname=envlog user=envlog host=psql password=alma")
cur = conn.cursor()
cur.execute('select unit, title from units where type=%s', (args.typ,))
unit, name = cur.fetchone()
title = '{}, sensor is {}, unit is {}'.format(name, args.sen, unit)
cur.execute(
        '''select temp from log 
        where type=%s and name=%s 
        order by insdate desc limit 65;''',
        (args.typ, args.sen)
        )
raw_data = cur.fetchall()
cur.close()
conn.close()
data = [item[0] for item in raw_data]

font = ImageFont.truetype(font_file, font_size)
inkyphat.set_colour('yellow')
inkyphat.text((border_bottom, 0), title, inkyphat.BLACK, font)

#Now math.
font_w, font_h = font.getsize('0') 
font_half_h = int(font_h / 2)

#How much vertical space we have for the chart?
#1 line for the title, half line for the tick label
#half because the label written at the middle of the tick
#two margins, one for separating the tick label from the
# border, the other to separate the chart and the title.
draw_h = int(inkyphat.HEIGHT - (font_h * 1.5) - (border_bottom * 2))

#Where to start to draw?
#Max y is at the bottom, so this is a large positive number
#Title, the margin between the title and the chart and the
#chart itself
draw_y = font_h + draw_h + border_bottom
data_min = min(data)
multi = draw_h / (max(data) - data_min)
tick_max_w = 0

#Drawing the ticks and labels and dotted lines
for x in range(0, draw_h, font_h * 2):
    real_x = inkyphat.HEIGHT - x - border_bottom  - font_half_h 
    inkyphat.line([(0, real_x), (1, real_x)], fill=inkyphat.BLACK, width=1)
    text = str(round((x / multi) + data_min, 1))
    tick_w, tick_h = font.getsize(text)
    if tick_w > tick_max_w:
        tick_max_w = tick_w
    inkyphat.text((border_bottom, real_x - (tick_h / 2)), text, inkyphat.BLACK, font)
    for point in range(inkyphat.WIDTH - 1, border_bottom + tick_h, -2):
        inkyphat.putpixel((point, real_x), inkyphat.RED)

#Drawing the chart
prev_x = inkyphat.WIDTH
prev_y = draw_y - int((data[0] - data_min) * multi)
for i in range(len(data)):
    act_x = inkyphat.WIDTH - (i*3)
    act_y = draw_y - int((data[i] - data_min) * multi)
    if act_x > tick_max_w + border_bottom: 
        inkyphat.line([(prev_x, prev_y), (act_x, act_y)], 
                inkyphat.BLACK, width=2)
    prev_x = act_x
    prev_y = act_y

#Displaying all this thing.
inkyphat.set_border(inkyphat.RED)
inkyphat.show()
