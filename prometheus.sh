#!/bin/bash

curl -s http://toll0.tomka.heji/metrics | 
    grep sensor_value | 
    grep -v TYPE | 
    tr -d "}" | 
    awk -F\" '{print $2,$NF}' | 
    sed 's/^.*_temperature/temp/; s/^.*_humidity/hum/; s/^.*_pressure/atm/; s/^.*_gas_resistance/gas/' | 
    while read name value 
    do 
        curl -s http://gw/templog/add/desk/"$name"/"$value" > /dev/null 
    done
