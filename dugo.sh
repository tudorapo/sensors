#!/bin/dash
#script to read the smart outlets
curl -s -u dugo:alma "http://dugo[0-4]/status" |
    jq -c '.meters[0].power, .temperature' |
    awk '
        BEGIN {
            names[1] = "server"
            names[2] = "media"
            names[3] = "sleep"
            names[4] = "desktop"
            names[5] = "various"
        }
        (NR%2 == 1) {print names[int(NR/2)+(NR%2)],"powr",$0} 
        (NR%2 == 0) {print names[int(NR/2)+(NR%2)],"dtmp",$0}
        ' |
    awk '{print "url = \"http://gw/templog/add/" $1 "/" $2 "/"$3"\"\n-o /dev/null"}' |
    curl -s -K - 

betap=`curl -s -u dugo:alma "http://betap/status" | jq .emeters[0].power`
curl -s "http://gw/templog/add/betap/inpw/$betap" -o /dev/null
