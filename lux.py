#!/usr/bin/python2

import smbus
import time
import math
import adafruit_mcp9808 as MCP9808

sensor = MCP9808.MCP9808()
sensor.begin()
sensor.clearConfigReg()
temp_sensor = '/sys/bus/w1/devices/28-03168377eeff/w1_slave'

def gettemp():
	f = open(temp_sensor, 'r')
	lines = f.readlines()
	f.close()
	if (lines[0].strip()[-3:] == 'YES') and (lines[1].find('t=') > 0):
		temp_string = lines[1].strip()[lines[1].find('t=')+2:]
		temp = float(temp_string) / 1000.0
		return temp

def log(szam):
	if szam == 0:
		return 0
	return math.log(szam)

bus = smbus.SMBus(1)
#this is the poweron, do not touch it.
bus.write_byte_data(0x39, 0x00 | 0x80, 0x03)
#this is the sensitivity:
# first two bits are the time used to collect the light,
# third bit is the sensitivity
# 000 =  13.7 ms, 1x
# 001 = 101   ms, 1x
# 010 = 402   ms, 1x
# 011 = manual, 1x
# 100 =  13.7 ms, 16x
# 101 = 101 ms, 16x
# 110 = 402 ms, 16x
# 111 = manual, 16x
bus.write_byte_data(0x39, 0x01 | 0x80, 0x02)
time.sleep(0.5)
data = bus.read_i2c_block_data(0x39, 0x0C | 0x80, 2)
data1 = bus.read_i2c_block_data(0x39, 0x0E | 0x80, 2)
ch0 = data[1] * 256 + data[0]
ch1 = data1[1] * 256 + data1[0]

print("slux %f lux" %log(ch0))
print("ilux %f lux" %log(ch1))
print("vlux %f lux" %log(ch0 - ch1))
print("room %f temp" %(sensor.readTempC()))
print("street %f temp" %(gettemp()))
