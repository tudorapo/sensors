#!/usr/bin/python2
import numpy as np
import soundfile as sf
import httplib
import logging
import subprocess

LOGFILE = "/tmp/sound.log"
BASE = 90
URL = "/templog/add/{:s}/sdb/{:f}"
HOST = "gw"
SNDFILE = "/tmp/test.wav"
SNDTIME = "300"

def send(subname,value):
    con = httplib.HTTPConnection(HOST)
    con.request("GET", URL.format(subname, value))
    res = con.getresponse()
    logging.debug(("Result {:d}/{:s} for " + URL).format(res.status, res.reason, subname, value))
    con.close()

logging.basicConfig(filename=LOGFILE, level=logging.DEBUG)

error = subprocess.call(["arecord", "-q", "-f", "S16_LE", "-D", "plughw:CARD=Device,DEV=0", "-d", SNDTIME, "-r", "44100", SNDFILE])
logging.debug("Arecord returned with {:d} code".format(error))

rms = sorted([20*np.log10(np.sqrt(np.mean(block**2))) for block in
               sf.blocks(SNDFILE, blocksize=1024, overlap=512)])
logging.debug("Send it in")
send("min", BASE + rms[0])
send("max", BASE + rms[-1])
send("avg", BASE + np.mean(rms))
