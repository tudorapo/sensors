#!/usr/bin/python2.7
import time
import argparse
from gpiozero import LED

parser = argparse.ArgumentParser(description='Blink a led according to the measured temperatue.')
parser.add_argument('-s', '--sensor', help='Unique component of /sysfs path of the sensor, aka 1wire \'MAC\'.', default='28-03168377eeff')
parser.add_argument('-l', '--led', help='Number of the GPIO pin on which the led is.', type=int, default=18)
parser.add_argument('-t', '--threshold', help='The number to subtract from the temperature', type=int, default=15)
parser.add_argument('-d', '--duration', help='The length of the blinks in seconds.', type=float, default=0.5)
parser.add_argument('-p', '--pause', help='The pause between the start of a series of blinks. Should the blinks last longer than this the pause will be 1 sec..', type=float, default=10)
args = parser.parse_args()

led = LED(args.led)
temp_sensor = '/sys/bus/w1/devices/'+args.sensor+'/w1_slave'

def gettemp():
    f = open(temp_sensor, 'r')
    lines = f.readlines()
    f.close()
    if (lines[0].strip()[-3:] == 'YES') and (lines[1].find('t=') > 0):
        temp_string = lines[1].strip()[lines[1].find('t=')+2:]
        temp = float(temp_string) / 1000.0
        return temp
    
while True: 
    temp = gettemp()
    print temp
    for i in range(int(temp)-args.threshold):
        led.on()
        time.sleep(args.duration)
        led.off()
        time.sleep(args.duration)
    pause = args.pause - ((int(temp) - args.threshold) * 2 * args.duration)
    if (pause < 1):
        pause = 1
    print pause
    time.sleep(pause)
