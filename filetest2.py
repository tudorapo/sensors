import inotifyx
import os
import time
from shutil import copyfile


fd = inotifyx.init()
try:
    wd = inotifyx.add_watch(fd, '/www', inotifyx.IN_MOVED_TO)
    while True:
        tick = time.time()
        events = inotifyx.get_events(fd)
        for event in events:
            if event.name == 'bubu.jpg':
                tock = time.time()
                print "event",tock - tick
                #need a separate directory so i can detect dir full events without 
                #halting raspistill
                timestring = time.strftime('/spool/tmp-%Y-%m-%d:%H:%M:%S.jpg')
                copyfile('/www/bubu.jpg',timestring)
                tack = time.time()
                print "image",tack - tock
                tick=tock
    inotifyx.rm_watch(fd, wd)
finally:
    os.close(fd)
