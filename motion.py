#!/usr/bin/python2.7

import threading
import time
import logging
import signal
import inotifyx
import os
import sys
import paramiko
import configargparse
from mem_top import mem_top
from gpiozero import LED, Button
from shutil import copyfile

class Motion:

    def __init__(self,args):
        self.args = args
        self.cycle = float(args.cycletime)
        self.dir = os.path.dirname(args.rawimage)
        self.file = os.path.basename(args.rawimage)
        self.logger = logging.getLogger('motion')
        self.gate = Button(args.gate,hold_time=0.1)
        self.led = LED(args.led)
        self.gate.when_pressed = self.breach
        self.gate.when_released = self.release
        self.led.on()
        #hit is the actual state of the gate, catch is if we caught a picture or not.
        #this is necessary for short breaches
        self.hit = threading.Event()
        self.catch = threading.Event()
        self.snap = threading.Event()
        self.ragnarok = threading.Event()
        if self.gate.is_pressed:
            self.logger.debug('Starting with pigeons')
            #in case the led is constantly blocked by a nesting pigeon.
            self.breach()

    def breach(self):
        """Watch for the IR sensor
        
        This starts at least one thread, hidden deeply in the gpiozero library.
        breach sets catch, but release does not clears it
        catch is cleared when we handled the picture
        """
        
        self.logger.info('Beam breached')
        self.hit.set()
        self.catch.set()
        self.led.off()

    def release(self):
        """Watch for the IR sensor
        
        This starts at least one thread, hidden deeply in the gpiozero library.
        """
        
        self.logger.info('Beam unbreached')
        self.hit.clear()
        self.led.on()

    def timewatcher(self):
        """This thread generates events when it's time for a snapshot"""
        
        self.logger.info('Timewatch starts')
        prev = int(time.time()/self.args.snaplen)
        while not self.ragnarok.is_set():
            cur = int(time.time()/self.args.snaplen)
            if prev != cur:
                self.logger.debug('snap set')
                self.snap.set()
                if self.args.memory:
                    logging.warning(mem_top(verbose_types=[dict, list]))
            prev = cur
            time.sleep(self.cycle)
        self.logger.info('Timewatch ends')

    def filewatcher(self):
        """This thread watches for new pictures

        Raspistill is making new pictures continously, this checks for them
        via inotifyx and if there is a new one checks for events.
        """
        
        self.logger.info('Filewatch begins')
        fd = inotifyx.init()
        try:
            wd = inotifyx.add_watch(fd, self.dir, inotifyx.IN_MOVED_TO)
            while not self.ragnarok.is_set():
                events = inotifyx.get_events(fd,self.cycle)
                if events and events[0].name == self.file:
                    self.logger.debug('Event found')
                    self.eventhandler()
                else:
                    self.logger.debug('Timeout')
            inotifyx.rm_watch(fd, wd)
        except:
            self.logger.error('Unexpected error:', sys.exc_info()[0])
            raise
        finally:
            os.close(fd)
        self.logger.info('Filewatch ends')

    def copywatcher(self):
        """This thread checks the spool directory for new pictures
        
        If any found, copies them to the sftp server.
        On the current hardware this is slower than raspistill so
        if there is constant activity this will fell behind, the spool dir
        fills up and an alert is generated.
        """
        
        self.logger.info('Copythread starts')
        stepback = 1
        key =  paramiko.rsakey.RSAKey(filename=self.args.sftpkey)
        while not self.ragnarok.is_set():
            filelist = os.listdir(self.args.spooldir)
            if len(filelist) == 0:
                time.sleep(self.cycle)
                self.logger.debug('Nothing to copy')
                continue
            starttime = time.time()
            self.logger.info('%d files to copy',len(filelist))
            try:
                ssh = paramiko.Transport((self.args.sftphost, self.args.sftpport))
                ssh.connect(username = self.args.sftpuser, pkey = key)
            except:
                self.logger.error('%d. Unexpected ssh error: %s\nWaiting for %f seconds.', stepback, sys.exc_info()[0],self.cycle*stepback)
                time.sleep(self.cycle*stepback)
                if self.cycle*stepback< 60:
                    stepback = stepback + 1
            else:
                stepback = 1
                sftp = paramiko.SFTPClient.from_transport(ssh)
                for pic in filelist:
                    if self.ragnarok.is_set():
                        break
                    srcpath = os.path.join(self.args.spooldir,pic)
                    dstpath = os.path.join(self.args.sftpdir,pic)
                    try:
                        sftp.put(srcpath,dstpath,confirm=True)
                    except:
                        self.logger.error('Unexpected error: %s', sys.exc_info()[0])
                    else:
                        os.remove(srcpath)
                sftp.close()
                ssh.close()
                self.logger.info('Files copied in %f seconds.',(time.time()-starttime))
            self.logger.info('One copy cycle ends.')
        self.logger.info('Copythread ends')

    def eventhandler(self,):
        """Checks for events when there is a new file"""
        
        if self.catch.is_set():
            if not self.args.nomotion:
                self.picture('moti')
                self.snap.clear()
            else:
                self.logger.debug('Motion picture was not taken.')
            #if we had a hit but have not recorded a picture yet
            if not self.hit.is_set():
                self.logger.debug('catch clearing')
                self.catch.clear()
        if self.snap.is_set():
            self.picture('snap')
        #clearing because we dont want delayed snaps after a series of motion pics
        self.snap.clear()

    def picture(self,type):
        """Copies the raw picture over
        
        With timestamp in the filename. This is the function which alerts when
        the spool directory is full.
        """
        
        self.logger.info('Picture of type %s copied.', type)
        timestring = time.strftime('-%Y-%m-%d:%H:%M:%S.jpg')
        dstpath = os.path.join(self.args.spooldir,type + timestring)
        srcpath = os.path.join(self.dir, self.file)
        try:
            copyfile(srcpath,dstpath)
        except IOError as err:
            self.logger.warning('Unable to write to %s, error was: %s' % 
                    (self.args.spooldir, err.strerror))
            #to avoid 0 byte long files
            os.remove(dstpath)

    def kill(self,signum,frame):
        """Kills threads when sigint or sigterm comes."""

        self.logger.info('Signal handler called with signal %s', signum)
        self.ragnarok.set()
        self.logger.info('Ragnarok set, massacre starts.')
        for t in threading.enumerate():
            self.logger.info('Living threads: %s', t.getName())
            if 'watch' in t.getName():
                self.logger.debug('Thread %s joined.', t.getName())
                t.join()
                self.logger.debug('Thread %s ended.', t.getName())
        self.logger.info('Massacre done, exiting.')

if __name__ == "__main__":
    p = configargparse.ArgParser()
    p.add('-c', '--config', help='Configuration file.', is_config_file=True, required=True)
    p.add('--sftpuser',     help='Username for sftp.', required=True)
    p.add('--sftphost',     help='Hostname to attach to sftp files.', required=True)
    p.add('--sftpport',     help='Ssh port.', type=int, default=22)
    p.add('--sftpkey',      help='Private part of the ssh key pair.', required=True)
    p.add('--sftpdir',      help='Remote ddirectory where to upload the files.',required=True)
    p.add('--gate',         help='The gpio pin for the IR sensor.', type=int, default=18)
    p.add('--led',          help='Led to blink on a catch.', type=int, default=27)
    p.add('--rawimage',     help='Image file to watch and copy.', required=True)
    p.add('--spooldir',     help='Spool directory where the interesting pictures will be stored before sftp.', required=True)
    p.add('--cycletime',    help='Internal cycle time.', default='0.8')
    p.add('--nomotion',     help='If the motion pictures are not needed, use this.', action='store_true')
    p.add('--snaplen',      help='Seconds between snapshots', type=int, default=500)
    p.add('--sftpdebug',    help='Set loglevel from WARNING to DEBUG for sftp.', action='store_true')
    p.add('-d','--debug',   help='Set loglevel from WARNING to DEBUG.', action='store_true')
    p.add('-v','--verbose', help='Set loglevel from WARNING to INFO.', action='store_true')
    p.add('-m','--memory',  help='Turn on memleak logging.', action='store_true')
    p.add('-l','--logfile', help='Logfile.')
    args=p.parse_args()

    format = '(%(threadName)-10s) by (%(name)-15s) at (%(levelno)-2s) %(asctime)s %(message)s'
    if args.logfile:
        logging.basicConfig(filename=args.logfile,format = format)
    else:
        logging.basicConfig(format = format)
    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)
        for key,arg in vars(args).iteritems():
            logging.debug("{} = {}".format(key,arg))
    elif args.verbose:
        logging.getLogger().setLevel(logging.INFO)
    else:
        logging.getLogger().setLevel(logging.WARNING)
    if args.sftpdebug:
        logging.getLogger("paramiko").setLevel(logging.DEBUG)
    else:
        logging.getLogger("paramiko").setLevel(logging.WARNING)

    logging.info('Start')
    m = Motion(args)
    signal.signal(signal.SIGINT,m.kill)
    signal.signal(signal.SIGTERM,m.kill)
    tf = threading.Thread(name='filewatch', target=m.filewatcher)
    tf.start()
    tt = threading.Thread(name='timewatch', target=m.timewatcher)
    tt.start()
    tc = threading.Thread(name='copywatch', target=m.copywatcher)
    tc.start()
    while not m.ragnarok.is_set():
        time.sleep(1)
    logging.info('End')
