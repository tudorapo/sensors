#!/usr/bin/env python3
"""Serving turbostat to prometheus"""

from subprocess import Popen, PIPE
from shlex import split
from threading import Thread
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
import re
import sys
import argparse

logging.basicConfig(level=logging.INFO)
parser = argparse.ArgumentParser(description='Turbostat for prometheus.')
parser.add_argument('-a', '--address', default='0.0.0.0',
                    help='ip address where to serve')
parser.add_argument('-p', '--port', default='9101',
                    help='Port where to serve')
args = parser.parse_args()

TITLE = "turbostat"

NAMES = []
NUMBERS = []

class Metrics(BaseHTTPRequestHandler):
    """Simple webserver."""
    def hwrite(self, msg):
        """for convenience."""
        self.wfile.write(bytes(msg, 'utf-8'))
    def log_request(self, code='-', size='-'):
        if code != 200:
            self.log_message('jaj! "%s" %s %s',self.requestline, str(code), str(size))
    def do_GET(self):
        """http reply."""
        if NUMBERS:
            self.send_response(200)
            self.send_header('Content-type', 'text/plain; version=0.0.4; charset=utf-8')
            self.end_headers()
            self.hwrite(f'# HELP { TITLE }_wattage performance in watts\n')
            self.hwrite(f'# TYPE { TITLE }_wattage gauge\n')
            for i, name in enumerate(NAMES):
                self.hwrite(f'{ TITLE }_wattage{{sensor="{ name.lower() }"}} { NUMBERS[i] }\n')
        else:
            self.send_response(503)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.hwrite('Wait a bit')

def web():
    """Simple webserver thread."""
    server = HTTPServer((args.address, int(args.port)), Metrics)
    logging.info('webserver starts at %s/%s', args.address, args.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()

def collect():
    """Collecting data."""
    global NUMBERS, NAMES
    cmd = '/usr/src/linux-headers-6.6.11-1-liquorix-amd64/tools/power/x86/turbostat/turbostat\
            -S -q -i 1 -s power'
    with Popen(split(cmd), stdout=PIPE) as proc:
        logging.info('turbostart start')
        number = re.compile('^[0-9.]*$')
        while True:
            line = proc.stdout.readline()
            if line:
                tmp = line.decode('utf8').strip().split('\t')
                if number.match(tmp[0]):
                    NUMBERS = tmp
                else:
                    NAMES = tmp
            else:
                logging.error('Opps turbostat lost')
                sys.exit(1)

if __name__ == "__main__":
    try:
        coll = Thread(target=collect)
        coll.daemon = True
        coll.start()
        web = Thread(target=web)
        web.daemon = True
        web.start()
        while coll.is_alive() and web.is_alive():
            coll.join(1)
    except KeyboardInterrupt:
        logging.info('turbostat ends')
        sys.exit(1)
