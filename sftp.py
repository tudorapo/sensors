import paramiko
import logging
import time
import os
import sys
start = time.time()
#logging.getLogger("paramiko").setLevel(logging.WARNING)
paramiko.util.log_to_file('/tmp/paramiko.log')
print "afterlog",time.time()-start
key =  paramiko.rsakey.RSAKey(filename='/home/pi/.ssh/sftp_key')
ssh = paramiko.Transport(('192.168.2.1', 22))
ssh.connect(username = 'cam', pkey = key)
print "afterconn",time.time()-start
sftp = paramiko.SFTPClient.from_transport(ssh)
mydir = '/spool'
theirdir = '/home/tudor/'
for i in os.listdir(mydir):
    j = os.path.join(mydir,i)
    k = os.path.join(theirdir,i)
    try:
        sftp.put(j,k,confirm=True)
    except:
        print "Unexpected error: %s", sys.exc_info()[0]
        print i
    else:
        print "suxess"    
print "afterput",time.time()-start
sftp.close()
ssh.close()
print "afterclose",time.time()-start
