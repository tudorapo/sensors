#!/usr/bin/python3
import board
import busio
import adafruit_sht31d
import time
import requests

i2c = busio.I2C(board.SCL, board.SDA)
sensor = adafruit_sht31d.SHT31D(i2c)
n = 0

while True:
    requests.get(f'http://gw/templog/add/extra/temp/{sensor.temperature}')
    requests.get(f'http://gw/templog/add/extra/hum/{sensor.relative_humidity}')
    time.sleep(30)
    sensor.heater = True
    time.sleep(1)
    sensor.heater = False
    time.sleep(30)
