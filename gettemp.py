#!/usr/bin/python2.7
temp_sensor = '/sys/bus/w1/devices/28-03168377eeff/w1_slave'
f = open(temp_sensor, 'r')
lines = f.readlines()
f.close()
if (lines[0].strip()[-3:] == 'YES') and (lines[1].find('t=') > 0):
	print(float(lines[1].strip()[lines[1].find('t=')+2:])/1000)

