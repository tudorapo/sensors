#!/usr/bin/python2.7
import time
import sys
from gpiozero import LED

led = LED(int(sys.argv[1]))
#led = LED(18)
while True:	
	led.on()
	time.sleep(0.5)
	led.off()
	time.sleep(0.5)
