import time
from gpiozero import LED

led = LED(18)
temp_sensor = '/sys/bus/w1/devices/28-03168377eeff/w1_slave'

def gettemp():
	f = open(temp_sensor, 'r')
	lines = f.readlines()
	f.close()
	if (lines[0].strip()[-3:] == 'YES') and (lines[1].find('t=') > 0):
		temp_string = lines[1].strip()[lines[1].find('t=')+2:]
		temp = float(temp_string) / 1000.0
		return temp
	
while True:	
	temp = gettemp()
	print temp
	if (temp > 25):
		led.on()
	else:
		led.off()
	time.sleep(1)


