#!/bin/sh
sudo rmmod bcm2835_v4l2 ; 
sudo modprobe bcm2835_v4l2

cvlc v4l2:///dev/video0\
    --v4l2-width $1 --v4l2-height $2 --v4l2-chroma h264 --v4l2-fps 10 \
    --http-host 0.0.0.0 --http-port 9998 --no-sout-audio \
    --sout '#standard{access=http,mux=ts}'
