#!/bin/dash
#script to read the smart outlets
for D in 0 1 2 3 4; do
    DATAFILE=`mktemp`
    curl -m 3 -s -u dugo:alma "http://dugo$D/status" |
        jq -c '.meters[0].power, .temperature' > $DATAFILE
    if [ -s $DATAFILE ] ; then
        cat $DATAFILE 
    else
       echo 3.14
       echo 3.14
    fi
done | 
    awk '
        BEGIN {
            names[1] = "server"
            names[2] = "media"
            names[3] = "sleep"
            names[4] = "desktop"
            names[5] = "various"
            names[6] = "test"
        }
        (NR%2 == 1) {print names[int(NR/2)+(NR%2)],"powr",$0} 
        (NR%2 == 0) {print names[int(NR/2)+(NR%2)],"dtmp",$0}
        ' | 
    awk '
        BEGIN {print "# TYPE outlet_power gauge"; print "# HELP outlet_power stuffing"}
        {print "outlet_power{outlet=\"" $1 "\",type=\"" $2 "\"}",$3}' | 
    sponge /var/lib/prometheus/node-exporter/outlet_power.prom

betap=`curl -s -u dugo:alma "http://betap/status" | jq .emeters[0].power`
echo "outlet_power{outlet=\"betap\",type=\"powr\"} $betap" >> /var/lib/prometheus/node-exporter/outlet_power.prom
/sbin/apcaccess | 
    grep -E "LOADPCT|BCHARGE|TIMELEFT" | 
    tr "[A-Z]" "[a-z]" | 
    awk -v hostname="$HOSTNAME" '{print "battery{battery=\"" hostname "\",type=\"" $1 "\"}",$3}'|
    sponge /var/lib/prometheus/node-exporter/battery.prom
