#!/usr/bin/env python3
import board
import busio
import requests
import adafruit_mcp9808
import adafruit_tsl2561

i2c = busio.I2C(board.SCL, board.SDA)
intemp = adafruit_mcp9808.MCP9808(i2c)
lux = adafruit_tsl2561.TSL2561(i2c)
temp_sensor = '/sys/bus/w1/devices/28-03168377eeff/w1_slave'

def gettemp():
    f = open(temp_sensor, 'r')
    lines = f.readlines()
    f.close()
    if (lines[0].strip()[-3:] == 'YES') and (lines[1].find('t=') > 0):
        temp_string = lines[1].strip()[lines[1].find('t=')+2:]
        temp = float(temp_string) / 1000.0
        return temp

def senddata(name, tipus, value):
    resp = requests.get(f'http://gw/templog/add/{name}/{tipus}/{value}')

senddata('room', 'temp', intemp.temperature)
#senddata('street', 'temp', gettemp())
feny = lux.lux
if feny is None:
    if lux.luminosity == (0,0): 
        feny = 0
    else:
        feny = 65000
senddata('room', 'ilux', feny)
