#!/usr/bin/python2.7

import SocketServer
import threading
import time
import logging
import signal
import os
import sys
import re
from Queue import Queue
from gpiozero import LED
import argparse

class BlinkyTCP(SocketServer.BaseRequestHandler):
    """
    TCP Server class

    This class was cargo culted from the documentation, changed some names
    and added the queue mechanism.
    """

    def handle(self):
        self.data = self.request.recv(1024).strip()
        logging.debug("{} wrote".format(self.client_address[0]))
        try:
            self.server.q.put_nowait(self.data)
        except:
            self.request.sendall('Queue is full, message dropped\n')
            logging.debug('Full queue, message dropped.')
        else:
            reply = 'Received %d items.\n' % len(self.data.split(' '))
            self.request.sendall(reply)

class Blinky:
    """
    Blinky, the general purpose led blinker class.

    Methods:

    do_blink - the actual led manipulation, see its doc for commands.
    blinky   - the thread receiving messages, running in an endless loop.
    kill     - tries to kill the server and the blinky thread.

    Variables:

    server   - a TCP server coming from SocketServer
    leds     - an array of gpiozero LED objects
    q        - a qeueu for message passing/buffering
    counter  - number of messages handled, for auditing purposes
    flag     - a semaphore to signal an end of the run.

    Basic usage:

    b = Blinky([LIST, OF, LED, GPIO, NUMBERS], hostname, portnumber)

    b.kill has to be set up a a SIGINT handler,
    b.blinky should be started as a new thread,
    and b.server should be started and we're in business.
    
    Problems:

    It is unable to clean up the TIME_WAIT tcp connections when exiting.

    Currently i'm using it to handle "negative connected" leds, which means
    that its working exactly the opposite as the writers of the gpiozero
    imagined, so led.on() will switches the leds off.
    """

    def __init__(self,ledlist,host,port):
        """
        Blinky constructor

        Parameters:

        ledlist - array of integers. These should be gpio ports doing 
            useful things via gpiozero.LED.
        host    - a hostname string, passed to SocketServer.TCPServer.
        port    - integer, tcp port for SocketServer.TCPServer.
        """

        self.server = SocketServer.TCPServer((host, port), BlinkyTCP)
        self.leds = dict()
        i = 0
        for ledno in ledlist:
            self.leds[i] = LED(ledno)
            #R00t made me to turn around the leds working.
            self.leds[i].on()
            i = i+1
        self.q = Queue(maxsize=20)
        self.server.q = self.q
        self.counter = 0
        self.flag = threading.Semaphore(0)

    def do_blink(self,message):
        """
        Led controlling

        This method checks if a message is a valid one, via a simple regexp,
        splices it to words then executes the words. 

        Words can be two kinds:
        
        l0 - a letter 'l' followed by an integer (without withespace) will 
            change the working led to the integerth led in the list. 
            Currently only accepted on the beginning of the line, but as
            soon as i write a better regexp, leds can be changed during
            message time.

        0.0 - a floating point number. The odd numbers in the message are the 
            light up phases and the even numbers are the dark phases. The
            number is the length of the phase in seconds. 

        Messages and words are checked against time limits. 
        
        Problems:

        A better regexp to allow in-message led switches.

        Make the time limits more visible and configurable

        The amount of indent suggests that this could be sliced up.
        """

        reg=re.compile('^[l0-9 .]+$')
        if reg.match(message):
            logging.debug('Valid message %d starts' % self.counter)
            blink = True
            duration = 0.0
            led = 0
            for word in message.split(' '):
                if word[0] == 'l':
                    try:
                        led = int(word[1:])
                    except ValueError:
                        logging.debug('Led change to invalid value %s', word)
                    else:
                        if led < 0:
                            led = 0
                        if led >= len(self.leds):
                            led = len(self.leds)-1
                        logging.debug('New led value is %d', led)
                        continue
                try:
                    times = float(word)
                except ValueError:
                    logging.debug('Valid message contains false value %s', word)
                    continue
                if times > 10.0:
                    logging.debug('Valid message contains too large value %f', times)
                    continue
                if blink:
                    self.leds[led].off()
                else: 
                    self.leds[led].on()
                time.sleep(times)
                blink = not blink
                #just to be sure.
                self.leds[led].on()
                duration = duration + times
                if duration > 60.0:
                    logging.debug('Valid message last longer than a minute, stopping.')
                    break
            logging.debug('Valid message %d ends' % self.counter)
        else:
            logging.debug('The message looks invalid')

    def blinky(self):
        """
        Message transfer and housekeeping

        This method is the endless loop which receives the messages from 
        the tcp server and kills all the participants which need killing.
        
        Problems:

        Does not closes tcp_waits.
        Tried join, but without much success. 
        """

        logging.debug('Blinky starts')
        while True:
            if self.flag.acquire(False):
                logging.debug('Blinky closes the server')
                self.server.server_close()
                logging.debug('Blinky kills the server')
                self.server.__shutdown_request = True
                time.sleep(0.6)
                logging.debug('Blinky ends')
                break
            try:
                message = self.q.get_nowait()
            except: 
                time.sleep(0.1)
            else:
                self.counter = self.counter + 1
                logging.debug("Blinky got %d message: %s" % (self.counter, message))
                if message == 'quit':
                    self.flag.release()
                self.do_blink(message)

    def kill(self,signum, frame):
        """
        The Terminator

        This method should be called via the ctrl-c signal. It sets the flag
        which commands blinky to self-terminate, then wirtes out some debug 
        info, then terminates itself.

        I think if i would do a join to the blinky thread in the for loop,
        the problem with the time_wait could be fixed, as this would give 
        time for the server_close and shutdown to do its work.
        """

        logging.debug('Signal handler called with signal %s', signum)
        self.flag.release()
        for t in threading.enumerate():
            logging.debug('Living threads: %s', t.getName())
            if t.getName() != 'MainThread':
                logging.debug('Thread %s joined.', t.getName())
                t.join()
                logging.debug('Thread %s ended.', t.getName())
        sys.exit(0)

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='A daemon to blink leds.')
    parser.add_argument('-l', '--log', '--logfile',help='A file to log events. If its not given it goes to stderr.')
    parser.add_argument('-a', '--host', help='The ip/hostname to attach to. The default is to attach to every interface.', default='')
    parser.add_argument('-p', '--port', help='The tcp port.', type=int, default=9999)
    parser.add_argument('-g', '--led', '--gpio', help='The GPIO port which controls a led. Just list the leds if you have more than one.', type=int, nargs='+')
    args = parser.parse_args()

    format = '(%(threadName)-10s) %(asctime)s %(message)s'

    if args.log :
        logging.basicConfig(level=logging.DEBUG,
            filename = args.log,
            format = format,)
    else:
        logging.basicConfig(level=logging.DEBUG,
            format = format,)
    
    if not args.led:
        logging.error('Please provide leds with the --led option!')
        sys.exit(-1)

    b = Blinky(args.led, args.host, args.port)
    signal.signal(signal.SIGINT,b.kill)
    try:
        t = threading.Thread(name='blinky', target=b.blinky )
        t.start()
    except:
        #sorry
        logging.error('Baj.')
    b.server.serve_forever()
    logging.debug('I dont think we ever get here.')
